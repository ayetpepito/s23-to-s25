let numberA = null;
let numberB = null;
let operation = null;


let inputDisplay = document.querySelector('#txt-input-display');
let btnNUmbers = document.querySelectorAll('.btn-numbers');

let btnAdd = document.querySelector('#btn-add');
let btnSubtract = document.querySelector('#btn-subtract');
let btnMultiply = document.querySelector('#btn-multiply');
let btnDivide = document.querySelector('#btn-divide');
let btnEqual = document.querySelector('#btn-equal');
let btnDecimal = document.querySelector('#btn-decimal');

let btnClearAll = document.querySelector('#btn-clear-all');
let btnClearEntry = document.querySelector('#btn-clear-entry');

let eqaulsClicked = false;

btnNUmbers.forEach(function(btnNumber){
// ES6 function
// .onclick - event listener
	btnNumber.onclick = () => {

		if(eqaulsClicked == false) {
			inputDisplay.value += btnNumber.textContent;
		}
		else {
			//remove the value from the input tag
			inputDisplay.value = null;//delete inputtagg
			inputDisplay.value += btnNumber.textContent;//add
			eqaulsClicked = false;//reset
		}
		}
	// inputDisplay.value += btnNumber.textContent;
	}

);

// vanilla js function

/*function functionName() {
inputDisplay.value += btnNumber.textContent;
}
*/

btnAdd.onclick = () => {
	if(numberA == null) {
	// stores the number or a value to a variable for future use
		numberA = inputDisplay.value;
		// stores a string  to be used  a condition for the equals button
		operation = 'addition';
		// resets the input tag to blank
		inputDisplay.value = null;
		}
	else if(numberB == null) {
	// stores the 2nd number value to another variable
		numberB = inputDisplay.value;
		// will add the 1st and 2nd number
		// numberA = numberA + numberB;
		numberA = Number(numberA) + Number(numberB)
		operation = 'addition';
		inputDisplay.value = null;
	}
}

btnSubtract.onclick = () => {
	if(numberA == null) {
	// stores the number or a value to a variable for future use
		numberA = inputDisplay.value;
		// stores a string  to be used  a condition for the equals button
		operation = 'subtraction';
		// resets the input tag to blank
		inputDisplay.value = null;
		}
	else if(numberB == null) {
	// stores the 2nd number value to another variable
		numberB = inputDisplay.value;
		// will add the 1st and 2nd number
		// numberA = numberA + numberB;
		numberA = Number(numberA) - Number(numberB)
		operation = 'subtraction';
		inputDisplay.value = null;
	}
}

btnDivide.onclick = () => {
	if(numberA == null) {
	// stores the number or a value to a variable for future use
		numberA = inputDisplay.value;
		// stores a string  to be used  a condition for the equals button
		operation = 'division';
		// resets the input tag to blank
		inputDisplay.value = null;
		}
	else if(numberB == null) {
	// stores the 2nd number value to another variable
		numberB = inputDisplay.value;
		// will add the 1st and 2nd number
		// numberA = numberA + numberB;
		numberA = Number(numberA) / Number(numberB)
		operation = 'division';
		inputDisplay.value = null;
	}
}

btnMultiply.onclick = () => {
	if(numberA == null) {
	// stores the number or a value to a variable for future use
		numberA = inputDisplay.value;
		// stores a string  to be used  a condition for the equals button
		operation = 'multiplication';
		// resets the input tag to blank
		inputDisplay.value = null;
		}
	else if(numberB == null) {
	// stores the 2nd number value to another variable
		numberB = inputDisplay.value;
		// will add the 1st and 2nd number
		// numberA = numberA + numberB;
		numberA = Number(numberA) * Number(numberB)
		operation = 'multiplication';
		inputDisplay.value = null;
	}
}

// btnSubtract.onclick = () => {
// 	if (numberA == null) {
// 		numberA = inputDisplay.value;
// 		operation = 'subtraction';
// 		inputDisplay.value = null;
// 	}
// 	else if(numberB == null) {
// 	// stores the 2nd number value to another variable
// 		numberB = inputDisplay.value;
// 		// will add the 1st and 2nd number
// 		// numberA = numberA + numberB;
// 		numberA = Number(numberA) - Number(numberB)
// 		operation = 'subtraction';
// 		inputDisplay.value = null;
// }

// btnEqual.onclick = () => {
// //conditional sttament that saves the 2nd number
// 	if(numberB === null && inputDisplay !== ''){
// 		numberB = inputDisplay.value;
// 	}
// 	inputDisplay.value = Number(numberA) + Number(numberB);
// };

btnEqual.onclick = () => {
//conditional sttament that saves the 2nd number
	if(numberB === null && inputDisplay !== ''){
		numberB = inputDisplay.value;
		}
		if (operation == 'addition') {
	inputDisplay.value = Number(numberA) + Number(numberB)}
	
		if (operation == 'subtraction') {
	inputDisplay.value = Number(numberA) - Number(numberB)}

		if (operation == 'multiplication') {
	inputDisplay.value = Number(numberA) * Number(numberB)}

		if (operation == 'division') {
	inputDisplay.value = Number(numberA) / Number(numberB)}
	numberA = null;
	numberB = null;
	operation =null;
}

btnClearAll.onclick = ()=> {
	//process
	// 1.clear all number entries
	numberA = null;
	numberB = null;
	// 2. clear input tag
	inputDisplay.value = null;
	// 3. clear operation
	operation = null;
}

// btnClearEntry.onclick = () => {
// 	inputDisplay.value = inputDisplay.value.substr(0, inputDisplay.value.length - 1);
// }


btnClearEntry.onclick = () => {
	inputDisplay.value = inputDisplay.value.slice(0, -1);
}


btnDecimal.onclick = () => {
//process
// 1. check if input is a decimal
// 2. if yes - nothing
// 3. if no - add decimal to input
  	if(!inputDisplay.value.includes('.')){
  		inputDisplay.value += '.';
  		// inputDisplay.value = inputDisplay.value+btnDecimal.textContent;
  		// inputDisplay +=btnDecimal.textContent;
  	}
}