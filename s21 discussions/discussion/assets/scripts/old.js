let numberA = null;
let numberB = null;
let operation = null;

let inputDisplay = document.querySelector('#txt-input-display');
console.log(inputDisplay);
//inputDisplay is variable
//document is a html file (index.html)
// .querySelector - target a specific element, retrun 1 element only
// always string with # - '#txt-input-display'

let btnNumbers = document.querySelectorAll('.btn-numbers');
console.log(btnNumbers);
// call several elements

let btnAdd = document.querySelector('.btn-add');
let btnSubtract = document.querySelector('.btn-subtract');
let btnMultiply = document.querySelector('.btn-multiply');
let btnDivide = document.querySelector('.btn-divide');
let btnEqual = document.querySelector('.btn-equal');
let btnDecimal = document.querySelector('.btn-decimal');

let btnClearAll = document.querySelector('.btn-clear-all');
let btnBackspace = document.querySelector('.btn-backspace');

console.log(btnAdd);
console.log(btnSubtract);
console.log(btnMultiply);
console.log(btnDivide);
console.log(btnEqual);
console.log(btnDecimal);
console.log(btnClearAll);
console.log(btnBackspace);

// es6 function
btnNumbers.forEach(function(btnNumber) {
	btnNumber.onclick = () => {
		inputDisplay.value += btnNumber.textContent;
		// keyvalue pairs - btnNumber.textContent
	}
});

// = () - function - es6
// vanilla js function
// function functionName () {
// 	inputDisplay.value += btnNumber.textContent;
// }
// btnNumber.onclick = function(){ is equivalent to btnNumber.onclick = () => { 
// .onclick - event listener - for every button (1-9) giving an event lister
// .textContent - text inside html element

// 07302020
btnAdd.onclick = () => {
	if (numberA == null) {
	//process
		// stores the value to a variable for future use
		numberA = inputDisplay.value;
		// stores a string to be use as condition for the equals button
		operation = 'addition';
		inputDisplay.value = null;
	}
	else if (numberB == null) {
		//stores the second value to another variable
		numberB = inputDisplay.value;
		//will add the 1st and 2nd number/previous numbers
		numberA = numberA + numberB;
		operation = 'addition';
		inputDisplay.value = null;
	}
}


btnEqual.onclick = () => {
	if(numberB == null && inputDisplay !== '') {
		numberB = inputDisplay.value;
	}
	inputDisplay.value = number(numberA) + number(numberB);

}