// console.log("hello");
let textdisplay = document.querySelector('#display');
console.log("parang tanga"+textdisplay.textContent);
// querySelector - targets Id

// let buttons2 = document.querySelectorAll('.button');
// console.log(buttons+"buttons1");
// querySelectorAll - targets classes/several elements

// let buttons1 = document.querySelector('.button');
// console.log(buttons1);
// targets 1st class

// let button = {
// 	accesskey: "",
// 	ariaAtomic: null,
// 	textContent: "button 1"
// }

// console.log(button.textContent);

let buttonB = document.querySelector('.buttonB');
console.log(buttonB.textContent);

// event listeners
buttonB.onclick = function test() {
	console.log("hello");
}


textdisplay.onchange = function test2() {
	console.log(textdisplay.value);
}

let buttonA = document.querySelector('.buttonA');

let arr = [];

//vanilla js
// buttonA.onclick = function() {
// 	arr.push(textdisplay.value);
// 	console.log(arr);
// }

// anonymous function
// () => {}

//es6 function
// buttonA.onclick = () => {
// 	arr.push(textdisplay.value);
// 	console.log(arr);
// }

// function addElement () {
// 	arr.push(textdisplay.value);
// 	console.log(arr);
// }

// buttonA.onclick = addElement();

let buttons = document.querySelectorAll('.button');

buttons.forEach(function(button) {
	button.onclick = function print () {
		console.log(textdisplay.value);
	}
})