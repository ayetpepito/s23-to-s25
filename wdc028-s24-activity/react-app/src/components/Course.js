import React, { useState } from 'react';//coursecard
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';//importing prop typr

export default function Course ({ courseProp }) {
	const {name, description, price} = courseProp;

	// State named count giving value of 0, setcount, is getter and setter, status of component
	// count state = 0;
	// setCoun, setter starts at "set" 
	// let noOfSeats = 10;
	const [count, setCount] = useState(0);
	const [countSeats, setSeat] = useState(10);
	
	// const [isOpen, setIsOpen] = useState(false);
	// const [name, setName] = useState('juan');
	// to manipulate, click enroll and price will get higher
	// synchronous and asynchronous functions


	function enroll() {
		setCount(count + 1);
		setSeat(countSeats -1 );
		console.log('Enrollees: ' + count);
		console.log('Seats: ' + countSeats);
	}

	return (
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Text>
					<span className="subtitle">Description</span>
					<br />
					{description}
					<br />
					<span className="subtitle">Price: </span>
					PHP {price}
					<br />
					<span className="subtitle">Enrollees: </span>
					{count}
					<br />
					<span className="subtitle">Seats: </span>
					{countSeats}
				</Card.Text>
				{countSeats ? 
				<Button className="bg-primary" onClick={enroll}>Enroll</Button>
				:
				<Button className="bg-danger" disabled>Not Available</Button>
				}
			</Card.Body>
		</Card>
	)
}
// onClick={enroll} to call function

//shape used to check that the prop conforms to a specific shape
//validator
Course.propTypes = {
	courseProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
		// seats: PropTypes.number.isRequired
	})
}

// Create a state hook on the CourseCard component representing the number of available seats
// Modify the enrollment function where a seat will be taken and the number of enrollees will go up on clicking the ‘Enroll’ button.
// Disable and change the color and text of the ‘Enroll’ button once seats reaches zero. (Ternary Operator)