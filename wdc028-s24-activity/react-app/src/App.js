import React from 'react';
import NavBar from './components/NavBar';
import './App.css';
//import Home from './pages/Home';
import Courses from './pages/Courses';

function App() {
  return (
    <React.Fragment>
    <NavBar />
      <Courses />
      </React.Fragment>
  );
}

export default App;