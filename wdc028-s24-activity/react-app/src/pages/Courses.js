import React from 'react';
import courseData from '../data/courses.js';
import Course from '../components/Course';

export default function Courses () {
	const courses = courseData.map(indivCourse => {
		return (
			<Course 
				key={indivCourse.id}
				courseProp={indivCourse}
			/>
		);			
	})

	return (
		<React.Fragment>
			{courses}
		</React.Fragment>
	)
}