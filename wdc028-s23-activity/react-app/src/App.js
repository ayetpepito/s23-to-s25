import React from 'react';
import NavBar from './components/NavBar';
import './App.css';
import Home from './pages/Home';
import Course from './components/Course';

function App() {
  return (
    <React.Fragment>
    <NavBar />
      <Home />
      <Course />
      </React.Fragment>
  );
}

export default App;