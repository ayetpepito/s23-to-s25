import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import { Container } from 'react-bootstrap';
import Course from '../components/Banner';

export default function Home () {
	return (
		<Container>
			<Banner />
			<Highlights />
			<Course />
		</Container>
	)
}