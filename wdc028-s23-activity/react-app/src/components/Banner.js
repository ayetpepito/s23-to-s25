import React from 'react';
// import Jumbotron from 'react-bootstrap/Jumbotron';
// import Button from 'react-bootstrap/Button';
// import { Button } from 'react-bootstrap';
import { Col, Row, Jumbotron,  Button } from 'react-bootstrap';
//import Row from 'react-bootrap/Row';

export default function Banner () {
	return (
		<Row>
			<Col>
				<Jumbotron>
					<h1>Zuitt Coding Bootcamp</h1>
					<p>Opportunities for everyone, everywhere</p>
					<Button className="bg-primary">Enroll now!</Button>
				</Jumbotron>
			</Col>
		</Row>
	)
}