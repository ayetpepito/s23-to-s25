import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
// import NavBar from './NavBar';

//import the Bootstrap  CSS
// import 'bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

//Process for creating and rendering components
//1. create a component - navbar - another file
//2. import the component - import Nav from 'react-bootstrap/Nav';
//3. render/call the component

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);