import React from 'react';
import NavBar from './components/NavBar';
import './App.css';
//import Home from './pages/Home';
// import Courses from './pages/Courses';
// import Register from './pages/Register';
import Login from './pages/Login';

function App() {
  return (
    <React.Fragment>
    <NavBar />
      <Login />
      </React.Fragment>
  );
}

export default App;