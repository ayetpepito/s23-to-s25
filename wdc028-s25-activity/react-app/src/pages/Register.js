import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';

export default function Register() {
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	// const input = document.querySelector('');
	function registerUser (e) {
		e.preventDefault();

		setEmail('');
		setPassword1('');
		setPassword2('');

		console.log("Thank you for registering!");
	}

	useEffect(() => {
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
		setIsActive(true);
		}else {
			setIsActive(false)
		}
	}, [email, password1, password2]);

	return (
		<Form onSubmit={e => registerUser(e)} className="col-lg-4 offset-lg-4 my-5">
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter Email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					//e is an event, with anonymous function
					// one way binding -received value in 1 way 
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			{isActive ? 
				<Button 
					className="bg-primary"
					type="submit" 
					id="submitBtn"
				>
					Submit
				</Button>
				:
				<Button 
					className="bg-danger"
					type="submit"
					id="submitButton"
					disabled
				>
					Submit
				</Button>
			}
			
		</Form>
	)
};