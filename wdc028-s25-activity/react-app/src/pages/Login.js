// 1. Create a Login page that will simulate actual user authentication.
// 2. The form must use 2-way binding.
// 3. Clear the input fields upon submission.
// 4. On successful login, console log a message saying “You have successfully logged in”

// Stretch Goal:
// if email isn't juantamad@gmail.com then console log a message saying "incorrect username and password" and it will not reset the input fields to blank 

import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';

export default function Login () {
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');

	function loginUser (e) {
		e.preventDefault();

		
		if(email !== "juantamad@gmail.com")  {

		console.log("Incorrect Username and Password!")
		}else {
			setEmail('');
			setPassword1('');
			console.log("You have successfully logged in!")
		}
	}

return (
		<Form onSubmit={e => loginUser(e)} className="col-lg-4 offset-lg-4 my-5">
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter Email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					//e is an event, with anonymous function
					// one way binding -received value in 1 way 
					required
				/>
				<Form.Text className="text-muted">
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter Password"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Button 
				className="bg-primary"
				type="submit" 
				id="submitBtn"
				>
				Submit
			</Button>
			
		</Form>
	)
};

